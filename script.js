document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent (event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

const isSharp = char => char === '#'
const getLength = text => text.length
const joinText = arrText => arrText.join('')
const addParagraph = text => `<p>${text}</p>`
const addHeader = size => text => `<h${size}>${text}</h${size}>`
const addBold = text => `<b>${text}</b>`
const getSizeHeader = R.compose(getLength, R.takeWhile(isSharp))
const setFormatTextInHeader =  R.compose(joinText, R.dropWhile(isSharp))

function setContentToHTML(text) {
	const sizeHeader = getSizeHeader(text)
	if (sizeHeader === 0) {
		return addParagraph(text)
	} else {
		const addHearderBySize = addHeader(sizeHeader)
		return addHearderBySize(setFormatTextInHeader(text))
	}
}

function parseMarkDown(content) {
	return content
		.split('\n\n')
		.map(setContentToHTML)
		.join('')
}
